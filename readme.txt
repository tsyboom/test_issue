=Installation requirements=

Python == 3.8;
Pipenv == 11.9.0

=Installation instruction=

1) CD to project folder;
2) run terminal - "pipenv shell";
3) run terminal - "pipenv install --ignore-pipfile";
4) edit db settings in settings.py (if you need);
5) run terminal - "manage.py makemigrations";
6) run terminal - "manage.py migrate";
7) run termianl - "manage.py createsuperuser";

=Instruction=

1) run manage.py runserver (for start server);
2) follow the link http://127.0.0.1:8000/admin/;
3) log in by superuser;
4) custom views -> file_view;
5) choose csv-file and upload.