from django.db import models


class User(models.Model):
    FirstName = models.CharField(max_length=30)
    LastName = models.CharField(max_length=30)
    BirthDate = models.CharField(max_length=30)
    RegistrationDate = models.CharField(max_length=30)


class Order(models.Model):
    CreatedDate = models.DateField(auto_now_add=True)
    ItemName = models.CharField(max_length=30)
    Client = models.OneToOneField(User, on_delete=models.CASCADE, related_name='order')
