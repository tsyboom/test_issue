from django import forms
import csv
from .serializers import UserSerializer


def csv_parser(file):
    reader = csv.DictReader(file.read().decode().splitlines())
    return reader


class FileForm(forms.Form):
    serializer = None
    upload_file = forms.FileField()

    def clean_upload_file(self):
        data = csv_parser(self.cleaned_data['upload_file'])
        self.serializer = UserSerializer(data=list(data), many=True)
        if self.serializer.is_valid():
            return self.serializer

    def save(self):
        self.serializer.save()
