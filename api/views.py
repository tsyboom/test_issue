from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.contrib import admin
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer, OrderSerializer
from .models import User, Order
from .forms import FileForm


class UserView(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


class OrderView(ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()


@admin.site.register_view('/add-users/')
def file_view(request):
    if request.POST:
        form = FileForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.save()
            return redirect('/admin')
    else:
        form = FileForm()
    context = {'form': form}
    return TemplateResponse(request, 'index.html', context=context)
